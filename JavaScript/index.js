var map = L.map('map').setView([41.49313,74.7091728], 8);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© OpenStreetMap'
}).addTo(map);

var marker = L.marker([42.8802281,74.593586]).addTo(map);
