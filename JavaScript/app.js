gsap.to('.image', {x: 70, duration: 2 })
gsap.fromTo('.header__navbar', {opacity: 0, y: 0}, {opacity: 1, y: 20, duration: 1.5})

const tl = gsap.timeline({defaults: {duration: 0.85}})
tl.fromTo('.main__button', {scale: 0}, {scale: 1,  ease: "power4.out",  ease: "bounce.out", duration: 1.8})
tl.fromTo('.add-to-cart', {opacity: 0,}, {opacity: 1,},'<80%')
tl.fromTo('.main__title', {y: 0, opacity: 0}, {y: 6, duration: 1, opacity: 1, }, '<')


const button = document.querySelector('.main__button')
button.addEventListener('click', () =>{
   gsap.to('.image', {opacity: 0, x: 180, duration: 1.5, ease: 'power4.out'})
})
